locals {
  # name_prefix = "${local.name_prefix}-bastion"
  name_prefix = "${var.metadata["prefix"]}-bastion"
  environment = var.metadata["environment"]
  id          = var.metadata["id"]

  role                         = "Bastion"
  internal_network_id          = var.internal_network["id"]
  internal_network_name        = var.internal_network["name"]
  internal_network_subnet_id   = var.internal_network["subnet_id"]
  internal_network_cidr        = var.internal_network["cidr"]
  management_network_id        = var.management_network["id"]
  management_network_name      = var.management_network["name"]
  management_network_subnet_id = var.management_network["subnet_id"]
  management_network_cidr      = var.management_network["cidr"]
  bastion_start_ip             = 19
  internal_monitoring_sg       = lookup(var.internal_sg, "monitoring", "monitoring")   #var.internal_sg["monitoring"]
  internal_management_sg       = lookup(var.management_sg, "management", "management") #var.internal_sg["monitoring"]
  internal_mail_sg             = lookup(var.internal_sg, "mail", "mail")
  internal_mail_sender_sg      = lookup(var.internal_sg, "mailsender", "mailsender")
}

# Keypair
resource "openstack_compute_keypair_v2" "instance" {
  name       = "${replace("srcp_bastion_${local.environment}_${local.id}", "-", "_")}_keypair"
  public_key = file("${var.private_key_file}.pub")
}

# Network Security Groups
resource "openstack_networking_secgroup_v2" "instance" {
  name        = "${local.name_prefix}-sg"
  description = "${local.name_prefix} SG ${local.role} "
}

resource "openstack_networking_secgroup_rule_v2" "ssh" {
  count             = length(var.ssh_management)
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = var.ssh_management[count.index]
  port_range_min    = 22
  port_range_max    = 22
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

# ICMP access from anywhere
resource "openstack_networking_secgroup_rule_v2" "icmp" {
  direction         = "ingress"
  protocol          = "icmp"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_node_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_group_id   = local.internal_monitoring_sg
  security_group_id = openstack_networking_secgroup_v2.instance.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_mail" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 25
  port_range_max    = 25
  remote_group_id   = local.internal_mail_sender_sg
  security_group_id = local.internal_mail_sg
}
# Instances

resource "openstack_networking_port_v2" "bastion-internal-port" {
  name           = "${local.name_prefix}-internal-port"
  network_id     = local.internal_network_id
  admin_state_up = "true"
  # The security groups must be specified by ID and not name (as opposed to how they are configured with the Compute Instance).
  security_group_ids = [
    local.internal_mail_sg,
  ]
  # value_specs = "${map("binding:vnic_type","direct")}"
  fixed_ip {
    subnet_id = local.internal_network_subnet_id
    ip_address = cidrhost(
      local.internal_network_cidr,
      local.bastion_start_ip,
    )
  }
}

resource "openstack_networking_port_v2" "bastion-management-port" {
  name           = "${local.name_prefix}-management-port"
  network_id     = local.management_network_id
  admin_state_up = "true"
  # The security groups must be specified by ID and not name (as opposed to how they are configured with the Compute Instance).

  security_group_ids = [
    openstack_networking_secgroup_v2.instance.id,
    local.internal_management_sg,
  ]
  # value_specs = "${map("binding:vnic_type","direct")}"
  fixed_ip {
    subnet_id = local.management_network_subnet_id
    ip_address = cidrhost(
      local.management_network_cidr,
      local.bastion_start_ip,
    )
  }
}

resource "openstack_compute_instance_v2" "instance" {
  depends_on  = [openstack_compute_keypair_v2.instance]
  name        = local.name_prefix
  image_name  = var.image
  flavor_name = var.flavor
  key_pair    = openstack_compute_keypair_v2.instance.name

  # we are attaching sec_grps to ports not the instance
  # security_groups = [
  #   "management",
  #   "local"
  # ]

  # Attach to the management network
  network {
    port = openstack_networking_port_v2.bastion-management-port.id

    # name        = "${var.management_network}"
    access_network = "true"
  }

  # Attach to the internal network
  network {
    # name        = "${var.internal_network}"
    port           = openstack_networking_port_v2.bastion-internal-port.id
    access_network = "false"
  }

  # Provisioning
  user_data = file("${path.module}/scripts/bootstrap.sh")

  metadata = {
    # Assign an UID to this resource
    Description = "SRCP Bastion host"
    Environment = local.environment
    Role        = local.role
    Managed     = "true"
  }
  lifecycle {
    create_before_destroy = false
  }
}

# Create FIP if Static resource is not defined
resource "openstack_networking_floatingip_v2" "fip" {
  count = var.floating_ip == "" ? 1 : 0
  pool  = var.floating_ip_pool
}

resource "openstack_compute_floatingip_associate_v2" "fip" {
  depends_on  = [openstack_compute_instance_v2.instance]
  count       = var.floating_ip == "" ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.fip[count.index]
  instance_id = openstack_compute_instance_v2.instance.id
}

# Attach Static FIP if static resource is defined
resource "openstack_compute_floatingip_associate_v2" "fip_static" {
  depends_on  = [openstack_compute_instance_v2.instance]
  count       = var.floating_ip != "" ? 1 : 0
  floating_ip = var.floating_ip
  instance_id = openstack_compute_instance_v2.instance.id
}
