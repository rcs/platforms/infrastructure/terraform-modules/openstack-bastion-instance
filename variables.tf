# variable "environment" { 
#   type = "string"
#   description = "Environment"
# }
variable "private_key_file" {
  type        = string
  description = "Path to private SSH key file to use for provisioning VM instances"
}

variable "image" {
  type        = string
  default     = "CentOS-7-x86_64-GenericCloud"
  description = "OS Image Name for VM instances"
}

variable "flavor" {
  type        = string
  default     = "C1.vss.tiny"
  description = "Flavor Name for VM instances"
}

variable "user" {
  type        = string
  default     = "centos"
  description = "SSH user name"
}

variable "internal_network" {
  type        = map(string)
  description = "Internal Network map"
}

variable "management_network" {
  type        = map(string)
  description = "Management Network map"
}

variable "instance_count" {
  type        = string
  default     = 1
  description = "Number of instances to create"
}

# variable "prefix" {
#   type = "string"
#   description = "Name prefix for all elements"
# }
variable "floating_ip_pool" {
  type        = string
  description = "Floating IP pool"
}

variable "floating_ip" {
  type        = string
  description = "Floating IP"
}

variable "ssh_management" {
  type        = list(string)
  description = "List of SSh allowed Network/Host"
}

variable "internal_sg" {
  type        = map(string)
  description = "SG to attach to servers"
}

variable "management_sg" {
  type        = map(string)
  description = "SG to attach to servers"
}

variable "metadata" {
  type        = map(string)
  description = "Map containing all meta-data like prefix, environemnt, project ID"
}

