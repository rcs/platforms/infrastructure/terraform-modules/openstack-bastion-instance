output "instance_internal_ip" {
  value = [openstack_compute_instance_v2.instance.*.access_ip_v4]
}

output "floating_ip" {
  #  value = openstack_networking_floatingip_v2.fip.*.address
  value = [
    "${var.floating_ip == "" ?
      join(",", openstack_networking_floatingip_v2.fip.*.address) :
    var.floating_ip}"
  ]

}

output "id" {
  value = [openstack_compute_instance_v2.instance.*.id]
}

# "${join("",openstack_networking_floatingip_v2.fip.*.address)}"
