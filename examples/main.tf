locals {
  # Create UUID for this cluster
  uuid = "${uuid()}"

  # name_prefix     = "SRCP-platform-${var.environment}"
  name_prefix = "SRCP-${var.platform_id}"

  # Public network CUDN-Private
  srcp_public_fip_pool = "CUDN-Internet"

  # Public network CUDN-Public
  internet_network = "a6c666e2-ce7c-4a41-b0f3-87d7321f0215"

  # Management network CUDN Private
  srcp_management_fip_pool = "CUDN-Private"

  int_sec_group_map = {
    name = "int_secgrp_name"
    id   = "int_secgrp_id"
  }

  manage_sec_group_map = {
    name = "manage_secgrp_name"
    id   = "manage_secgrp_id"
  }

  internal_network_map = {
    name = "internal"
    id = "id"
    subnet_id = "id"
    router_external_ip = "1.2.3.4"
    cidr = "1.2.3.4/30"
  }

  management_network_map = {
    name = "management"
    id = "id"
    subnet_id = "id"
    router_external_ip = "1.2.3.4"
    cidr = "1.2.3.4/30"
  }

  srcp-data = {
    "environment" = var.environment
    "prefix"      = "srcp"
    "name"        = "SRCP-vHPC"
    "id"          = var.platform_id
  }
}

module "bastion" {
  source   = "../"
  metadata = local.srcp-data

  private_key_file   = "path/to/key"
  internal_network   = local.internal_network_map
  management_network = local.management_network_map
  ssh_management     = var.management_cidr_access
  internal_sg        = local.int_sec_group_map
  management_sg      = local.manage_sec_group_map

  # Floating IP is not static here
  floating_ip_pool = local.srcp_management_fip_pool
  floating_ip      = "0.0.0.0"

  # Images and flavours
  image = var.images["bastion"]
}
