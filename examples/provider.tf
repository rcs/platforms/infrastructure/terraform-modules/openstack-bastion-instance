provider "openstack" {
  version = "~> 1.7"
}
provider "null" {
  version = "~> 2.1"
}
provider "template" {
  version = "~> 2.1"
}

