terraform {
  backend "swift" {
    container         = "tf-bastion-terraform-state"
    archive_container = "tf-bastion-terraform-state-backup"
  }
}
