variable "environment" {
  default     = "dev"
  type        = string
  description = "Environment"
}

variable "account_id" {
  type        = string
  default     = "accountid"
  description = "Account ID"
}

variable "platform_id" {
  default     = "platformid"
  type        = string
  description = "Platform ID. Added as a meta-data and in SSH keys"
}

variable "images" {
  type        = map
  description = "Images"
}

variable "flavors" {
  type        = map
  description = "Flavors"
}

variable "management_cidr_access" {
  type        = list
  default     = []
  description = "List of CIDR addresses allowed to access the bastion host"
}

variable "public_network" {
  type        = string
  default     = "d579d662-47f0-4b78-bf4f-030fe07fe477"
  description = "Public Network"
}

variable "management_sg" {
  description = "description"
  default     = "default"
}

variable "floating_ip" {
  description = "description"
  default     = "default"
}

