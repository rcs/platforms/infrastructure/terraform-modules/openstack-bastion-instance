output "bastion_floating_ip" {
  value       = module.bastion.floating_ip
  description = "Bastion Floating IP"
}
